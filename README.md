Create and load the Database using
php artisan migrate
php artisan db:seed

To call the API use
<web host>/API/<State abbreviation>
or
<web host>/API/<State abbreviation>/<zip to ignore>

