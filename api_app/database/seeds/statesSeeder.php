<?php

use Illuminate\Database\Seeder;

class statesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('states')->insert([
            ['name' => 'North Carolina', 'abbreviation' => 'NC'],
            ['name' => 'California', 'abbreviation' => 'CA'],
        ]);
    }
}
