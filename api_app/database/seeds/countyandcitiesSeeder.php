<?php

use Illuminate\Database\Seeder;

class countyandcitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('countyandcities')->insert([
            ['county_name' => 'Brunswick', 'zip' => '28461','city_name' => 'Oak Island','state_id' => 1 ],
            ['county_name' => 'Brunswick', 'zip' => '28461','city_name' => 'Southport','state_id' => 1 ],
            ['county_name' => 'Brunswick', 'zip' => '28461','city_name' => 'Saint James','state_id' => 1 ],
            ['county_name' => 'Alamance', 'zip' => '27215','city_name' => 'Burlington','state_id' => 1 ],
            ['county_name' => 'Altamahaw', 'zip' => '27202','city_name' => 'Burlington','state_id' => 1 ],
            ['county_name' => 'Carolina Beach', 'zip' => '28428','city_name' => 'New Hanover','state_id' => 1 ],
            ['county_name' => 'Anaheim', 'zip' => '92850','city_name' => 'Orange','state_id' => 2 ] 
        ]);
    }
}
