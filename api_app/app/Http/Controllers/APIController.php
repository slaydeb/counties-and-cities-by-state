<?php

namespace App\Http\Controllers;
use App\states;
use App\countyandcities;
use Illuminate\Http\Request;

class APIController extends Controller
{
    //
    public function show($state)  {

        
        $displayState = states::where('abbreviation', $state)
              ->first();
        if (!$displayState)   {
            $response= $state . " was not found.";
            return view('notfound',['response'=>$response]);
        }
        
        $statename = $displayState->name;
        $state_id = $displayState->id;
       // if (!)
        $counties=countyandcities::where('state_id', $state_id)->get();
        
        $this->display( $counties, $statename); 
  

        
    }
    public function noZip ($state,$zip)  {
     $displayState = states::where('abbreviation', $state)
              ->first();
        if (!$displayState)   {
            $response= $state . " was not found.";
            return view('notfound',['response'=>$response]);
        }
        
        $statename = $displayState->name;
        $state_id = $displayState->id;
       // if (!)
        $counties=countyandcities::where([['state_id','=', $state_id],['zip','<>',$zip]])->get();
        
        $this->display( $counties, $statename);        
    }
    
    public function display( $counties,$statename) {
        $response="{ \"state\":\"" .  $statename . "\"";

        //find out how many counties passed
        $countyCount = 0;
         foreach ($counties as $county )   {
             $countyCount++;
         }
        //echo $countyCount . " counties passed";
        if ($countyCount >0) {
             $response=$response . ",\"counties\":[";
        }
        
        //$county="";
        $lastcounty="";
        $count=0;
        foreach ($counties as $county )   {
            $currentcounty=$county->county_name;
            //echo $currentcounty;
            if ($currentcounty!=$lastcounty)    {
                if ($lastcounty)    {
                    //exits so need to end 
                    $response=$response . "]},";
                }
                $response=$response . "{\"name\":\"" . $county->county_name . "\",\"cities\":[";
                
            } else {
                $response=$response . ",";
            }
            $response=$response ."{\"name\":\"" . $county->city_name ."\",";
            $response=$response ."\"zip\":\"" . $county->zip ."\"}";
                
            $lastcounty =  $currentcounty;
            $count++;
        }    
        if ( $countyCount >0) {
            //end cities array
            $response=$response . "]}]";
        }
                
        $response=$response . "}";  

        //return $displayState->name;
        echo $response;
        //return $response;
                
    }
}
